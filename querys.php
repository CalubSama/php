<?php
$sql = "SELECT * FROM productos";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table>
  <tr>
  <th>ID</th>
  <th>Nombre</th>
  <th>Descripcion</th>
  <th>Precio</th>
  <th>Codigo de Barras</th>
  <th>Imagen</th>
  <th>Unidad</th>
  <th>Perecedero</th>
  </tr>";
  
  while($row = $result->fetch_assoc()) {
    echo "<tr>
    <td>".$row["id"]."</td>
    <td>".$row["nombre"]."</td>
    <td>".$row["descripcion"]."</td>
    <td>".$row["precio"]."</td>
    <td>".$row["codigo_barras"]."</td>
    <td>".$row["imagen_producto"]."</td>
    <td>".$row["unidad_medida"]."</td>
    <td>".$row["es_perecedero"]."</td>
    </tr>";
  }
  echo "</table>";
} else {
  echo "No se encontraron resultados";
}
$conn->close();
?>
